<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220430203004 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        // **** NCI 27/05/2022 - SQL "up" commands are commented because already executed on PROD
        // **** TODO : uncomment after prod resynchronization
        //$this->addSql('CREATE TABLE commerce (id INT AUTO_INCREMENT NOT NULL, titre VARCHAR(1024) NOT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME DEFAULT NULL, description VARCHAR(10000) DEFAULT NULL, vignetteName VARCHAR(255) DEFAULT NULL, isVisible TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        //$this->addSql('CREATE TABLE livreur (id INT AUTO_INCREMENT NOT NULL, identite VARCHAR(1024) NOT NULL, description LONGTEXT DEFAULT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME DEFAULT NULL, vignetteName VARCHAR(255) DEFAULT NULL, isVisible TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        //$this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, password VARCHAR(255) NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE commerce');
        $this->addSql('DROP TABLE livreur');
        $this->addSql('DROP TABLE user');
    }
}
