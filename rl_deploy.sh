#!/bin/bash

if [ -d sav_vendor ]
then
    rm -fr sav_vendor
fi

if [ -d sav_vendor ]
then
    echo "Erreur, la purge de sav_vendor a echoue."
    exit -1
fi

if [ -d vendor ]
then
    mkdir sav_vendor
    mv vendor sav_vendor/
fi

if [ -d vendor ]
then
    echo "Erreur, la sauvegarde de vendor/ a echoue."
    exit -1
fi

rm -fr var/cache/*
composer install
php bin/console doctrine:migrations:migrate --no-interaction
php bin/console cache:clear

exit 0
