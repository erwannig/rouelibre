<?php

namespace App\DataFixtures;

use App\Entity\Livraisons\Commerce;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Exception;

class CommerceFixtures extends Fixture implements OrderedFixtureInterface
{
    /**
     * @throws Exception
     */
    public function load(ObjectManager $manager)
    {
        $this->loadClasse($manager,
            "Épicerie de Fontaine-Daniel",
            'epicerie.jpg',
            "Description"
        );
        $this->loadClasse($manager,
            "Biocoop Mayenne",
            'biocoop.jpg',
            "Description"
        );
        $this->loadClasse($manager,
            "Savons de Raphaël",
            'savons.jpg',
            "Description"
        );
        $this->loadClasse($manager,
            "Librairie du Marais",
            'marais.jpg',
            "Description"
        );

        $manager->flush();
    }

    public function loadClasse($manager, $titre, $img, $description){
        $commerce = new Commerce();
        $commerce->setTitre($titre);
        $commerce->setDescription($description);
        $manager->persist($commerce);
        $manager->flush();

        if($img !== ''){
            try {
                $commerce->setVignetteFile(new UploadedFile('public/medias/fixtures/' . $img, $img, null, null, true), false);
                $commerce->uploadVignetteImg(true);
            } catch (\Exception $e) {
            }
        }
    }

    /**
     * Get the order of this fixture
     *
     * @return int
     */
    public function getOrder()
    {
        // TODO: Implement getOrder() method.
        return 10;
    }
}
