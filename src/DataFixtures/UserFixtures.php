<?php

namespace App\DataFixtures;

use App\Entity\Livraisons\Livreur;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Exception;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture implements OrderedFixtureInterface
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @throws Exception
     */
    public function load(ObjectManager $manager)
    {
        $admin = new User();
        $password = "test";
        $admin->setEmail("admin@test.com");
        $admin->setPlainPassword($password);
        $admin->setPassword($this->passwordEncoder->encodePassword(
            $admin,
            $password
        ));
        $manager->persist($admin);
        $manager->flush();


        $this->loadUser($manager,
            "Mme Michu",
            true,
            "woman1.png"
            );
        $this->loadUser($manager,
            "Mme Bidule",
            true,
            "woman2.png"
        );
        $this->loadUser($manager,
            "Mr Truc",
            true,
            "man1.png"
        );
        $this->loadUser($manager,
            "Mr ZZtop",
            true,
            "man2.png"
        );
        $this->loadUser($manager,
            "Mr Genuflexion",
            true,
            "man3.png"
        );
        $this->loadUser($manager,
            "Mr Z",
            true,
            "man4.png"
        );
        $this->loadUser($manager,
            "Mme Knock",
            true,
            "woman3.png"
        );
        $this->loadUser($manager,
            "Mme Lajoie",
            true,
            "woman4.png"
        );
        $manager->flush();
    }

    public function loadUser($manager, $identite, $visible, $img){
        $user = new Livreur();
        $user->setIdentite($identite);
        $user->setVisible($visible);
        $manager->persist($user);
        $manager->flush();

        if($img !== ''){
            try {
                $user->setVignetteFile(new UploadedFile('public/medias/fixtures/' . $img, $img, null, null, true), false);
                $user->uploadVignetteImg(true);
            } catch (\Exception $e) {
            }
        }
    }

    /**
     * Get the order of this fixture
     *
     * @return int
     */
    public function getOrder()
    {
        // TODO: Implement getOrder() method.
        return 1;
    }
}
