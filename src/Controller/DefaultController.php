<?php


namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class DefaultController extends AbstractController
{
    /**
     * @Route("/",name="homepageV2022")
     * @return Response
     */
    public function indexV2022()
    {
        return $this->render("homepageV2022.html.twig", []);
    }
    
    /**
     * @Route("/coursiers",name="coursiers")
     * @return Response
     */
    public function coursiersAction()
    {
        return $this->render("coursiers.html.twig", []);
    }
    
    /**
     * @Route("/presentation",name="presentation")
     * @return Response
     */
    public function presentationAction()
    {
        return $this->render("presentation.html.twig", []);
    }
    
    /**
     * @Route("/velocargo",name="velocargo")
     * @return Response
     */
    public function velocargoAction()
    {
        return $this->render("velocargo.html.twig", []);
    }
    
    /**
     * @Route("/prepasorties",name="prepasorties")
     * @return Response
     */
    public function prepasortiesAction()
    {
        return $this->render("prepasorties.html.twig", []);
    }
    
    /**
     * @Route("/amenagements",name="amenagements")
     * @return Response
     */
    public function amenagementsAction()
    {
        return $this->render("amenagements.html.twig", []);
    }
    
    /**
     * @Route("/atelier",name="atelier")
     * @return Response
     */
    public function atelierAction()
    {
        return $this->render("atelier.html.twig", []);
    }
    
    /**
     * @Route("/projets",name="projets")
     * @return Response
     */
    public function projetsAction()
    {
        return $this->render("projets.html.twig", []);
    }
}
