<?php

namespace App\Controller\Admin;

use App\Entity\Livraisons\Commerce;
use App\Entity\Livraisons\Livreur;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use App\Controller\Admin\LivreurCrudController;

class DashboardController extends AbstractDashboardController
{
    // TODO : ne fonctionne pas en PROD - commenté en attendant d'avoir des versions ISO
    /*	 
    public function __construct(AdminUrlGenerator $adminUrlGenerator)
    {
        $this->adminUrlGenerator = $adminUrlGenerator;
    }
    */
    
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
    	  $url = $this->adminUrlGenerator->setController(LivreurCrudController::class)->generateUrl();
        return $this->redirect( $url );
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('TP Sur EasyAdmin');
    }

    public function configureMenuItems(): iterable
    {
        [
            yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home'),
            yield MenuItem::section( "Livraisons", "fa fa-book-open" ),
            yield MenuItem::linkToCrud('Commerces', 'fa fa-tags', Commerce::class),
            yield MenuItem::linkToCrud('Livreurs', 'fa fa-tags', Livreur::class)
        ];
    }
}
