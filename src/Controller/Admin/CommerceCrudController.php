<?php

namespace App\Controller\Admin;

use App\Entity\Livraisons\Commerce;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Vich\UploaderBundle\Form\Type\VichImageType;

class CommerceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Commerce::class;
    }


    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle(Crud::PAGE_INDEX, "Commerce")
            ->setPageTitle(Crud::PAGE_EDIT, "Edition du commerce");
    }

    public function configureFields(string $pageName): iterable
    {
        $imageView = ImageField::new( 'vignetteName' )->setBasePath( 'medias/images/commerces' )->setLabel( "Image" );
        $imageEdit = ImageField::new( 'vignetteFile' )->setFormType( VichImageType::class )->setFormTypeOptions(
            [
                'delete_label' => 'Supprimer l‘image',
                'download_uri' => false,
                'image_uri' => static function (Commerce $commerce) {
                    return $commerce->getWebPathImg();
                }
            ]
        );

        switch ($pageName) {
            case Crud::PAGE_NEW:
            case Crud::PAGE_EDIT:
                return [
                    TextField::new( 'titre' ),
                    TextEditorField::new('description'),
                    BooleanField::new( "visible" ),
                    $imageEdit
                ];
            default:
                return [
                    IdField::new( "id" ),
                    TextField::new( "titre" ),
                    $imageView
                ];
        }
    }

}
